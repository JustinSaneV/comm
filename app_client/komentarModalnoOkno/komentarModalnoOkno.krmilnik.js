(function() {
  /* global angular */
  
  function komentarModalnoOkno($uibModalInstance, edugeocachePodatki, podrobnostiLokacije, avtentikacija) {
    var vm = this;
    
    /* INICIALIZACIJA OBRAZCA */
    vm.seznamOcen = ['1', '2', '17', '4', '5'];
    // vm.izbranaOpcija = vm.seznamOcen[2];
    
    
    console.log("ocena:", vm.podatkiObrazca);
    
    // vm.zacetnaOcena = function () {
    //   console.log("zacetnaOcena");
    //   return 3;
    // }
    
    vm.podrobnostiLokacije = podrobnostiLokacije;
    
    vm.prejVsebinaKomentarja = vm.podrobnostiLokacije.vsebinaKomentarja;
    vm.ocenaPrej = vm.podrobnostiLokacije.prejOcena.toString(); // to dela
    // console.log("vm.ocenaPrej:", vm.ocenaPrej);
    var aliPosodobiKomentar = vm.podrobnostiLokacije.posodobiKomentar;
    vm.komentarId = vm.podrobnostiLokacije.komentarId;
    
    // console.log(vm.ocenaPrej);
    
    // console.log("prejOcena", vm.prejOcena);
    
    vm.modalnoOkno = {
      zapri: function(odgovor) {
        $uibModalInstance.close(odgovor);
      },
      preklici: function() {
        $uibModalInstance.close();
      }
    };
    
    vm.dodajKomentar = function(idLokacije, podatkiObrazca) {

      edugeocachePodatki.dodajKomentarZaId(idLokacije, {
        // naziv: podatkiObrazca.naziv,
        elektronskiNaslov: avtentikacija.trenutniUporabnik().elektronskiNaslov,
        ocena: podatkiObrazca.ocena,
        komentar: podatkiObrazca.komentar
      }).then(
        function success(odgovor) {
          vm.modalnoOkno.zapri(odgovor.data);
        },
        function error(odgovor) {
          vm.napakaNaObrazcu = "Napaka pri shranjevanju komentarja, poskusite znova!";
        }
      );
      return false;
    }
    
    vm.posodobiKomentar = function(idLokacije, komentarId, podatkiObrazca) {
      console.log("vm.posodobiKomentar podatkiObrazca:", komentarId);
      
      
      
      edugeocachePodatki.posodobiKomentarZaId(idLokacije, komentarId, {
        avtor: avtentikacija.trenutniUporabnik().ime,
        elektronskiNaslov: avtentikacija.trenutniUporabnik().elektronskiNaslov,
        ocena: podatkiObrazca.ocena,
        besediloKomentarja: podatkiObrazca.komentar
      }).then(
        function success(odgovor) {
          /* Najprej zbrisi starega */
          vm.podrobnostiLokacije.odstraniKomentarSSeznama(komentarId);
          vm.modalnoOkno.zapri(odgovor.data);
        },
        function error(odgovor) {
          console.log(odgovor);
          vm.napakaNaObrazcu = "Napaka pri posodabljanju komentarja, poskusite znova!";
        }
      );
      return false;
    }
    
 
    
    vm.posiljanjePodatkov = function() {
      vm.napakaNaObrazcu = "";
      if (!vm.podatkiObrazca.ocena || !vm.podatkiObrazca.komentar) {
        vm.napakaNaObrazcu = "Prosim, izpolnite vsa vnosna polja!";
        return false;
      } else {
        console.log(vm.podatkiObrazca);
        if (aliPosodobiKomentar) {
          console.log("POSODOBI KOMENTAR");
          // console.log("podatkiObrazca:", vm.podatkiObrazca);
          vm.posodobiKomentar(vm.podrobnostiLokacije.idLokacije, vm.komentarId, vm.podatkiObrazca);
        }
        else {
          console.log("DODAJ KOMENTAR", vm.podrobnostiLokacije.idLokacije, vm.podatkiObrazca);
          vm.dodajKomentar(vm.podrobnostiLokacije.idLokacije, vm.podatkiObrazca);
        }
      }
    };
  };
  komentarModalnoOkno.$inject = ['$uibModalInstance', 'edugeocachePodatki', 'podrobnostiLokacije', 'avtentikacija'];
  
  angular
    .module('edugeocache')
    .controller('komentarModalnoOkno', komentarModalnoOkno);
})();