(function() {
  /* global angular */
  
  registracijaCtrl.$inject = ['$location', 'avtentikacija'];
  function registracijaCtrl($location, avtentikacija) {
    var vm = this;
    
    vm.glavaStrani = {
      naslov: 'Kreiranje novega EduGeoCache uporabniškega računa'
    };
    
    vm.prijavniPodatki = {
      ime: "",
      elektronskiNaslov: "",
      geslo: ""
    };
    
    vm.prvotnaStran = $location.search().stran || '/';
    
    vm.posiljanjePodatkov = function() {
      vm.napakaNaObrazcu = "";
      if (!vm.prijavniPodatki.ime || !vm.prijavniPodatki.elektronskiNaslov || !vm.prijavniPodatki.geslo) {
        vm.napakaNaObrazcu = "Zahtevani so vsi podatki, prosim poskusite znova!";
        return false;
      } else {
        vm.izvediRegistracijo();
      }
    };
    
    vm.izvediRegistracijo = function() {
      vm.napakaNaObrazcu = "";
      avtentikacija
        .registracija(vm.prijavniPodatki)
        .then(function success() {
          $location.search('stran', null);
          $location.path(vm.prvotnaStran);
        }, function error(napaka) {
          console.log(napaka);
          vm.napakaNaObrazcu = napaka.data.sporocilo;
        });
    };
  }
  
  angular
    .module('edugeocache')
    .controller('registracijaCtrl', registracijaCtrl);
})();